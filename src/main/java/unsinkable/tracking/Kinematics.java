package unsinkable.tracking;

import robocode.util.Utils;
import unsinkable.debug.Logger;

import java.awt.*;
import java.awt.geom.AffineTransform;

/**
 * Represents an enemies Kinematics. Includes simple linear prediction.
 */
public class Kinematics {
    private final OwnMachine self;
    private double x;
    private double y;
    private double heading;
    private double velocity;
    private long time;
    private static final long BETA = 1;
    private double vx;
    private double vy;

    private Kinematics(OwnMachine self, double x, double y, double vx, double vy, double heading, double velocity, long time) {
        this.self = self;
        this.x = x;
        this.y = y;
        this.heading = heading;
        this.velocity = velocity;
        this.time = time;
        this.vx = vx;
        this.vy = vy;
    }

    public Kinematics(OwnMachine self) {
        this(self, 0, 0, 0, 0, 0, 0, 0);
    }

    /**
     * @param x
     * @param y
     * @return the distance of this object to given position (x,y)
     */
    private double getDistance(double x, double y) {
        double dx = x - getX();
        double dy = y - getY();
        double distance = Math.sqrt(dx*dx + dy*dy);

        return distance;
    }

    /**
     * @return the distance of this object to ourselves.
     */
    public double getDistance() {
        return getDistance(self.getX(), self.getY());
    }

    /**
     * Update this kinematics with given event-data.
     *
     * @param event
     */
    public void update(ScannedEnemyEvent event) {
        updateCartesianPosition(event);

        time = event.time;
        heading = event.heading;
        velocity = event.velocity;

        Logger.debug("Self heading=", Utils.normalAbsoluteAngleDegrees(Math.toRadians(self.getHeading())));
        Logger.debug("Enemy", event.name, "detected at (", (int) x, ", ", (int) y, ")",
                "[dist=", event.distance, ", brn=", Math.toDegrees(event.bearing), "]");
    }

    /**
     * Calculate new cartesian position based on given event.
     * @param event
     */
    private void updateCartesianPosition(ScannedEnemyEvent event) {
        double absoluteBearing = event.bearing + self.getHeading();
        double dx = event.distance * Math.sin(absoluteBearing);
        double dy = event.distance * Math.cos(absoluteBearing);
        long dt = event.time - time;

        double xNew = self.getX() + dx;
        double yNew = self.getY() + dy;
        double xPredict = x + vx * dt;
        double yPredict = y + vy * dt;

        vx = (xNew - getX()) / dt;
        vy = (yNew - getY()) / dt;

        x = self.getX() + dx;
        y = self.getY() + dy;

        double rx = x - xPredict;
        double ry = y - yPredict;

//        vx = vx + (BETA / dt) * rx;
//        vy = vy + (BETA / dt) * ry;
    }

    /**
     * @return the shape of this object, translated and rotated to its visible position (for graphical debugging).
     */
    public Shape getShape() {
        AffineTransform transformation = new AffineTransform();
        transformation.translate(x, y);
        transformation.rotate(-heading);

        return transformation.createTransformedShape(self.getShape());
    }

    /**
     *
     * @return the absolute bearing of this object, measured from ourselves (independent of our heading).
     */
    public double getBearing() {
        double bearing = Utils.normalAbsoluteAngle(Math.atan2(x - self.getX(), y - self.getY()));
        return bearing;

    }

    /**
     *
     * @return the relative bearing of this object, measured from ourselves (taking our heading into account)
     */
    public double getRelativeBearing() {
        double absBearing = getBearing();
        return Utils.normalRelativeAngle(absBearing - self.getHeading());
    }

    public double getX() {
        return x;
    }

    /**
     *
     * @param time
     * @return a new Kinematics object, based on extrapolation of this Kinematics object to given time
     */
    public Kinematics update(long time) {
        long dt = time - this.time;

        double dx = velocity * dt * Math.sin(heading);
//        double dx = vx * dt;
        double dy = velocity * dt * Math.cos(heading);
//        double dy = vy * dt;

        Kinematics predicted = new Kinematics(self, x + dx, y + dy, vx, vy, heading, velocity, time);

        Shape predictedShape = predicted.getShape();
        if (predictedShape.intersects(self.BORDER_BOTTOM)
                || predictedShape.intersects(self.BORDER_LEFT)
                || predictedShape.intersects(self.BORDER_RIGHT)
                || predictedShape.intersects(self.BORDER_TOP)) {
            predicted = new Kinematics(self, x + dx, y + dy, 0, 0, heading, 0, time);
        }

        return predicted;
    }

    public double getY() {
        return y;
    }

    /**
     *
     * @param gun
     * @return the absolute bearing of this object, measured from given gun.
     */
    public double getBearingCannon(Engagement.Gun gun) {
        if (Engagement.Gun.FRONT == gun) {
            return getBearingFrontCannon();
        } else {
            return getBearingBackCannon();
        }
    }

    private double getBearingFrontCannon() {
        double bearing = getBearing(self.getXFrontCannon(), self.getYFrontCannon());
        return bearing;
    }

    private double getBearingBackCannon() {
        double bearing = getBearing(self.getXBackCannon(), self.getYBackCannon());
        return bearing;
    }

    /**
     * @param x
     * @param y
     * @return the absolute bearing of this object, measured from given position (x,y)
     */
    private double getBearing(double x, double y) {
        double dx = getX() - x;
        double dy = getY() - y;

        return Math.atan2(dx, dy);
    }
}
