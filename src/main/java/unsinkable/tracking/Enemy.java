package unsinkable.tracking;

/**
 * Represents an enemy, which may be a Robot, or a Ship, depending on the context.
 */
public class Enemy {
    public final String name;
    private Kinematics kinematics;
    private long timestamp;
    private double energy;

    public Enemy(OwnMachine self, String name) {
        this.name = name;
        kinematics = new Kinematics(self);
    }

    public Kinematics getKinematics() {
        return kinematics;
    }

    /**
     * Update this enemy with a new observation.
     * @param event
     */
    public void update(ScannedEnemyEvent event) {
        timestamp = event.time;
        energy = event.energy;
        kinematics.update(event);
    }

    /**
     * Update the kinematics by predicting to given timestamp.
     * @param time
     */
    public void predict(long time) {
        kinematics = kinematics.update(time);
    }

    /**
     * @return the time of the last update (measurement) for this enemy.
     */
    public long getTimestamp() {
        return timestamp;
    }

}
