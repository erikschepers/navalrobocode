package unsinkable.tracking;

import robocode.ScannedRobotEvent;
import robocode.ScannedShipEvent;

/**
 * Abstraction of ScannedRobotEvent and ScannedShipEvent, to allow re-use of functionality in both naval (ship)
 * and non-navel robocode (tank).
 */
public class ScannedEnemyEvent {
    public final String name;
    public final double heading;
    public final double bearing;
    public final double distance;
    public final double energy;
    public final double velocity;
    public final long time;

    public ScannedEnemyEvent(ScannedRobotEvent e) {
        name = e.getName();
        heading = e.getHeadingRadians();
        bearing = e.getBearingRadians();
        distance = e.getDistance();
        energy = e.getEnergy();
        velocity = e.getVelocity();
        time = e.getTime();
    }

    public ScannedEnemyEvent(ScannedShipEvent e) {
        name = e.getName();
        heading = e.getHeadingRadians();
        bearing = e.getBearingRadians();
        distance = e.getDistance();
        energy = e.getEnergy();
        velocity = e.getVelocity();
        time = e.getTime();
    }
}
