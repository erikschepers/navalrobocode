package unsinkable.tracking;

/**
 * Represents an engagement, i.e. an active relation between a weapon, and an enemy.
 */
public class Engagement {
    public enum Gun {
        FRONT,
        BACK
    }

    private final Enemy target;
    private final Gun gun;

    public Engagement(Enemy target, Gun gun) {
        this.target = target;
        this.gun = gun;
    }

    public Enemy getTarget() {
        return target;
    }

    public Gun getGun() {
        return gun;
    }
}
