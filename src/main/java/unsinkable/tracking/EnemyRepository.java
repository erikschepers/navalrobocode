package unsinkable.tracking;

import robocode.util.Utils;

import java.util.*;

/**
 * Keeps track of all alive enemies in the current battlefield.
 */
public class EnemyRepository {
    private final Map<String, Enemy> enemyDb;
    private final OwnMachine self;

    public EnemyRepository(OwnMachine self) {
        this.self = self;
        enemyDb = new HashMap<String, Enemy>();
    }

    /**
     * Updates the enemy with given name in this repository with given observation. If
     * said enemy is not yet in the repository, it is added.
     *
     * @param name
     * @param event
     */
    public synchronized void updateEnemy(String name, ScannedEnemyEvent event) {
        Enemy enemy = enemyDb.get(name);
        if (enemy == null) {
            enemy = new Enemy(self, name);
            enemyDb.put(name, enemy);
        }
        enemy.update(event);
    }

    /**
     *
     * @param name
     * @return the enemy with given name from this repository, or null if there is no enemy
     * with that name.
     */
    public synchronized Enemy getEnemy(String name) {
        return enemyDb.get(name);
    }

    /**
     *
     * @param name
     * @return true if, and only if, the enemy with given name is in this repository.
     */
    public boolean contains(String name) {
        return enemyDb.containsKey(name);
    }

    /**
     *
     * @return list of enemies, sorted by distance to ourselves (closest first).
     */
    public List<Enemy> getAllEnemiesSortedByDistance() {
        List<Enemy> sortedByDistance = new ArrayList<Enemy>(enemyDb.values());
        Collections.sort(sortedByDistance, new Comparator<Enemy>() {
            @Override
            public int compare(Enemy o1, Enemy o2) {
                return (int) (o1.getKinematics().getDistance() - o2.getKinematics().getDistance());
            }
        });
        return sortedByDistance;
    }

    /**
     * Remove enemy with given name from the repository. This method should be called if
     * an enemy died.
     *
     * @param name
     */
    public synchronized void remove(String name) {
        enemyDb.remove(name);
    }

    /**
     *
     * @return the enemy that is the closest to ourselves.
     */
    public Enemy getNearestEnemy() {
        Enemy nearest = null;
        if (enemyDb.size() > 0) {
            nearest = getAllEnemiesSortedByDistance().get(0);
        }
        return nearest;
    }

    /**
     * Sorts all enemies in the repository by their relative bearing (originating from ourselves).
     * The bearing-gap between the first and the last enemy in the returned list is guaranteed
     * to be the largest.
     *
     * @return list of enemies sorted by bearing.
     */
    public List<Enemy> getAllEnemiesSortedByBearing() {
        List<Enemy> sortedByBearing = new ArrayList<Enemy>(enemyDb.values());
        Collections.sort(sortedByBearing, new Comparator<Enemy>() {
            @Override
            public int compare(Enemy o1, Enemy o2) {
                return (int) Math.toDegrees(Utils.normalRelativeAngle(o1.getKinematics().getBearing() - o2.getKinematics().getBearing()));
            }
        });
        // Find the biggest bearing-gap between 2 adjacent enemies, pick the one after the gap
        // as starting position
        double maxGap = 0;
        int maxGapIndex = 0;
        for (int i = 0; i < sortedByBearing.size(); i++) {
            int nextIndex = (i + 1) % sortedByBearing.size();
            double gap = Utils.normalAbsoluteAngle(sortedByBearing.get(nextIndex).getKinematics().getBearing()
                    - sortedByBearing.get(i).getKinematics().getBearing());
            if (gap > maxGap) {
                maxGap = gap;
                maxGapIndex = nextIndex;
            }
        }

        // The bearing gap between the last and first enemy in the list is the largest
        List<Enemy> resorted = new ArrayList<Enemy>(sortedByBearing.size());
        resorted.addAll(sortedByBearing.subList(maxGapIndex, sortedByBearing.size()));
        resorted.addAll(sortedByBearing.subList(0, maxGapIndex));
        return resorted;
    }

    /**
     * @return the update-time of the least recently updated enemy. If not all enemies have
     * been observed yet, then this method returns Integer.MIN_VALUE
     */
    public long getOldestUpdateTime() {
        long updateTime = self.getTime();
        if (self.getOthers() > enemyDb.size()) {
            updateTime = Integer.MIN_VALUE;
        } else {
            for (Enemy enemy : enemyDb.values()) {
                if (enemy.getTimestamp() < updateTime) {
                    updateTime = enemy.getTimestamp();
                }
            }
        }
        return updateTime;
    }
}
