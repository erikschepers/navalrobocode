package unsinkable.tracking;

import robocode.AdvancedRobot;
import robocode.Ship;

import java.awt.*;

/**
 * Abstraction of Tank/Ship, to allow re-use of functionality in both naval (ship) and non-navel robocode (tank).
 */
public class OwnMachine {
    private static final int BOT_LENGTH = 60;
    private static final int BOT_WIDTH = 40;
    private static final Rectangle BOT_SHAPE = new Rectangle(-BOT_WIDTH / 2, -BOT_LENGTH / 2, BOT_WIDTH, BOT_LENGTH);

    private static final int SHIP_LENGTH = 200;
    private static final int SHIP_WIDTH = 40;
    private static final Rectangle SHIP_SHAPE = new Rectangle(-SHIP_WIDTH / 2, -SHIP_LENGTH / 2, SHIP_WIDTH, SHIP_LENGTH);

    private static final int BORDER_ZONE = 200;
    public Rectangle BORDER_LEFT;
    public Rectangle BORDER_TOP;
    public Rectangle BORDER_RIGHT;
    public Rectangle BORDER_BOTTOM;

    private final AdvancedRobot delegateBot;
    private final Ship delegateShip;
    private final Shape shape;

    public OwnMachine(AdvancedRobot delegate) {
        delegateBot = delegate;
        delegateShip = null;
        shape = BOT_SHAPE;
    }

    public void setBorders(int width, int height) {
        BORDER_LEFT = new Rectangle(0, 0, BORDER_ZONE, height);
        BORDER_TOP = new Rectangle(0, height-BORDER_ZONE, width, BORDER_ZONE);
        BORDER_RIGHT = new Rectangle(width-BORDER_ZONE, 0, BORDER_ZONE, height);
        BORDER_BOTTOM = new Rectangle(0, 0, width, BORDER_ZONE);
    }

    public OwnMachine(Ship delegate) {
        delegateShip = delegate;
        delegateBot = null;
        shape = SHIP_SHAPE;
    }


    public double getX() {
        double x;
        if (delegateBot != null) {
            x = delegateBot.getX();
        } else {
            x = delegateShip.getXMiddle();
        }

        return x;
    }

    public double getY() {
        double y;
        if (delegateBot != null) {
            y = delegateBot.getY();
        } else {
            y = delegateShip.getYMiddle();
        }

        return y;
    }

    public double getHeading() {
        double heading;
        if (delegateBot != null) {
            heading = delegateBot.getHeadingRadians();
        } else {
            heading = delegateShip.getBodyHeadingRadians();
        }

        return heading;
    }

    public Shape getShape() {
        return shape;
    }

    public double getXFrontCannon() {
        double x;
        if (delegateBot != null) {
            x = delegateBot.getX();
        } else {
            x = delegateShip.getXFrontCannon();
        }
        return x;
    }

    public double getYFrontCannon() {
        double y;
        if (delegateBot != null) {
            y = delegateBot.getY();
        } else {
            y = delegateShip.getYFrontCannon();
        }
        return y;
    }

    public double getXBackCannon() {
        double x;
        if (delegateBot != null) {
            x = delegateBot.getX();
        } else {
            x = delegateShip.getXBackCannon();
        }
        return x;
    }

    public double getYBackCannon() {
        double y;
        if (delegateBot != null) {
            y = delegateBot.getY();
        } else {
            y = delegateShip.getYBackCannon();
        }
        return y;
    }

    public long getTime() {
        long time;
        if (delegateBot != null) {
            time = delegateBot.getTime();
        } else {
            time = delegateShip.getTime();
        }

        return time;
    }

    public int getOthers() {
        int others;
        if (delegateBot != null) {
            others = delegateBot.getOthers();
        } else {
            others = delegateShip.getOthers();
        }
        return others;
    }
}
