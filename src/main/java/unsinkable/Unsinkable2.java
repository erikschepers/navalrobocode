package unsinkable;

import robocode.*;
import robocode.naval.BlindSpot;
import robocode.util.Utils;
import unsinkable.debug.GraphicalDebugger;
import unsinkable.debug.Logger;
import unsinkable.tracking.*;

import java.awt.*;
import java.util.List;

/**
 * Battleship with:
 * - simple tracking
 * - sailing in circles, while avoiding the walls
 * - independent engagements for front and back gun
 * - mining when a ship is directly behind (see RamMiner)
 * - put radar in wiper-mode when all enemies are within a sector < 180 degrees, to have higher accuracy
 * - put radar in tracking-mode when only 1 enemy left
 */
public class Unsinkable2 extends Ship {
    public static final int MAXIMUM_TIME_ENEMY_NOT_SEEN = 50;
    /**
     * Keep track of other ships
     */
    private EnemyRepository shipRepository;
    /**
     * Representation of our own ship (abstraction of Ship/Tank).
     */
    private final OwnMachine self = new OwnMachine(this);
    /**
     * Used to draw lines and symbols on the battle-field, for graphical debugging
     * (requires "paint" to be enabled for this ship during the battle).
     */
    private final GraphicalDebugger debugger = new GraphicalDebugger(self);

    /**
     * Direction we are sailing in (1 = forward, -1 = going backwards)
     */
    private int sailDirection = 1;

    /**
     * The current engagement of the front-gun (or null if no active engagement).
     */
    private Engagement frontGunEngagement = null;
    /**
     * The current engagement of the front-gun (or null if no active engagement).
     */
    private Engagement backGunEngagement = null;
    /**
     * The direction the radar is turning (1 = clockwise, -1 = counter-clockwise)
     */
    private int scanDirection = 1;

    @Override
    public void run() {
        initialize();

        while(true){
            predictAllEnemies();
            updateScanDirection();
            move();

            frontGunEngagement = updateEngagement(frontGunEngagement, Engagement.Gun.FRONT);
            backGunEngagement = updateEngagement(backGunEngagement, Engagement.Gun.BACK);
            executeEngagement(frontGunEngagement);
            executeEngagement(backGunEngagement);

            // End turn
            execute();
        }
    }

    /**
     * Predict where all enemies are.
     */
    private void predictAllEnemies() {
        for (Enemy enemy: shipRepository.getAllEnemiesSortedByDistance()) {
            enemy.predict(getTime());
        }
    }

    /**
     * Sail in a circle, while avoiding walls by changing direction when getting close to the borders
     */
    private void move() {
        Shape myShape = debugger.getMyPosition();
        double heading = Utils.normalAbsoluteAngle(getBodyHeadingRadians());
        if (sailDirection < 0) {
            heading += Math.PI;
        }

        if (myShape.intersects(self.BORDER_BOTTOM)
                && Utils.normalAbsoluteAngle(1.5 * Math.PI - heading) < Math.PI) {
            sailDirection *= -1;
        } else if (myShape.intersects(self.BORDER_LEFT)
                && Utils.normalAbsoluteAngle(2 * Math.PI - heading) < Math.PI) {
            sailDirection *= -1;
        } else if (myShape.intersects(self.BORDER_TOP)
                && Utils.normalAbsoluteAngle(0.5 * Math.PI - heading) < Math.PI) {
            sailDirection *= -1;
        } else if (myShape.intersects(self.BORDER_RIGHT)
                && Utils.normalAbsoluteAngle(Math.PI - heading) < Math.PI) {
            sailDirection *= -1;
        }

        setAhead(sailDirection * 4000);
        setTurnRightRadians(Double.POSITIVE_INFINITY);
    }

    /**
     * Updates the radar scan-direction, based on the number of enemies left, and the distribution of
     * enemies on the battle field.
     * * If it's been a while since we've seen all enemies, perform a full scan (360)
     * * Else, if only 1 enemy left, track him
     * * Else, if all remaining enemies are within a sector < 180 degrees, then scan in wiper-mode
     */
    private void updateScanDirection() {
        if (getTime() - shipRepository.getOldestUpdateTime() > MAXIMUM_TIME_ENEMY_NOT_SEEN) {
            Logger.debug("NEED FULL SCAN!");
            setTurnRadarRightRadians(2 * Math.PI);
        } else if (getOthers() == 1) {
            Enemy other = shipRepository.getNearestEnemy();
            double enemyBearing = other.getKinematics().update(getTime()).getBearing();
            double dAngle = Utils.normalRelativeAngle(enemyBearing - getRadarHeadingRadians());
            setTurnRadarRightRadians(dAngle);
            Logger.debug("RADAR: Enemy-brn=", Math.toDegrees(enemyBearing),
                    " radar-heading=", Math.toDegrees(getRadarHeadingRadians()),
                    " turn-angle=", Math.toDegrees(dAngle));
        } else {
            List<Enemy> enemiesByBearing = shipRepository.getAllEnemiesSortedByBearing();
            if (enemiesByBearing.size() >= 2) {
                double scanZoneStart = enemiesByBearing.get(0).getKinematics().getBearing();
                double scanZoneEnd = enemiesByBearing.get(enemiesByBearing.size() - 1).getKinematics().getBearing();
                double scanZoneWidth = Utils.normalAbsoluteAngle(scanZoneEnd - scanZoneStart);
                if (scanZoneWidth < Math.toRadians(180)) {
                    double radarHeadingRelativeToScanZoneStart = Utils.normalRelativeAngle(getRadarHeadingRadians() - scanZoneStart);
                    double radarHeadingRelativeToScanZoneEnd = Utils.normalRelativeAngle(getRadarHeadingRadians() - scanZoneEnd);
                    if (radarHeadingRelativeToScanZoneStart < 0 || radarHeadingRelativeToScanZoneStart > scanZoneWidth) {
                        // outside scan-sector
                        if (radarHeadingRelativeToScanZoneStart < 0
                                && Math.abs(radarHeadingRelativeToScanZoneStart) < Math.abs(radarHeadingRelativeToScanZoneEnd)) {
                            scanDirection = 1;
                        } else {
                            scanDirection = -1;
                        }
                    }
                    Logger.debug("Scan sector: [", (int) Math.toDegrees(scanZoneStart),
                            ", ", (int) Math.toDegrees(scanZoneEnd), "] (",
                            (int) Math.toDegrees(scanZoneWidth),
                            ") --> ", (int) Math.toDegrees(radarHeadingRelativeToScanZoneStart),
                            " [dir=", scanDirection, "]");
                }
            }
            setTurnRadarRightRadians(scanDirection * 2 * Math.PI);
        }
    }

    /**
     *
     * @param gun
     * @return heading of given gun, in radians
     */
    private double getCannonHeadingRadians(Engagement.Gun gun) {
        if (Engagement.Gun.FRONT == gun) {
            return getFrontCannonHeadingRadians();
        } else {
            return getBackCannonHeadingRadians();
        }
    }

    /**
     * Turn the gun.
     * @param gun
     * @param angle
     */
    private void setTurnCannonRightRadians(Engagement.Gun gun, double angle) {
        if (Engagement.Gun.FRONT == gun) {
            setTurnFrontCannonRightRadians(angle);
        } else {
            setTurnBackCannonRightRadians(angle);
        }
    }

    /**
     * Fire given gun with given firepower.
     * @param gun
     * @param firepower
     */
    private void fireCannon(Engagement.Gun gun, double firepower) {
        if (Engagement.Gun.FRONT == gun) {
            //TODO: Should return a Bullet object, so that bullet-events can be related to this fired bullet
            fireFrontCannon(firepower);
        } else {
            fireBackCannon(firepower);
        }
    }

    /**
     * Execute given engagement (i.e. fire the gun on the enemy).
     * @param engagement
     */
    private void executeEngagement(Engagement engagement) {
        if (engagement != null) {
            Enemy target = engagement.getTarget();
            Kinematics targetPredict = target.getKinematics().update(getTime());
            double firepower = getFirePower(target);
            double bulletVelocity = getBulletVelocity(firepower);
            for (int i = 0; i < 5; i++) {
                double dtImpact = targetPredict.getDistance() / bulletVelocity;
                targetPredict = target.getKinematics().update(getTime() + (long) dtImpact);
            }

            double bearing = targetPredict.getBearingCannon(engagement.getGun());
            double distance = targetPredict.getDistance();
            if (!getBlindSpot(engagement.getGun()).inBlindSpot(bearing - getBodyHeadingRadians())) {
                double relativeGunAngle = Utils.normalRelativeAngle(bearing - getCannonHeadingRadians(engagement.getGun()));
                Logger.debug(engagement.getGun().name(), ": Bearing [", target.name, "] -> ", Math.toDegrees(bearing));
                Logger.debug(engagement.getGun().name(), ": heading -> ", Math.toDegrees(getCannonHeadingRadians(engagement.getGun())));
                Logger.debug(engagement.getGun().name(), ": relative-angle -> ", Math.toDegrees(relativeGunAngle));
                if (relativeGunAngle < 0) {
                    setTurnCannonRightRadians(engagement.getGun(), relativeGunAngle);
                } else {
                    setTurnCannonRightRadians(engagement.getGun(), relativeGunAngle);
                }
                if (Math.abs(relativeGunAngle) < Math.toRadians(1000 / distance)) {
                    Logger.debug(engagement.getGun().name(), ": FIRE!!!");
                    fireCannon(engagement.getGun(), firepower);
                }
            }
        }
    }

    /**
     *
     * @param target
     * @return a sensible firepower for given target, taking into account our own energy level.
     */
    private double getFirePower(Enemy target) {
        double power = 1000/target.getKinematics().getDistance();
        if (getEnergy() < 15) {
            power = Math.min(1, power);
        } else {
            power = Math.max(1, power);
        }
        return power;
    }

    /**
     * @param gun
     * @return the blindspot of given gun.
     */
    private BlindSpot getBlindSpot(Engagement.Gun gun) {
        if (Engagement.Gun.FRONT == gun) {
            return getCopyOfBlindSpotFrontCannon();
        } else {
            return getCopyOfBlindSpotBackCannon();
        }
    }

    /**
     * @param engagement
     * @param gun
     * @return an engagement for the closest enemy that we can hit with given gun, or null if we cannot hit anyone
     * with this gun.
     */
    private Engagement updateEngagement(Engagement engagement, Engagement.Gun gun) {
        // Always set-up new engagement
        for (Enemy enemy : shipRepository.getAllEnemiesSortedByDistance()) {
            if (!getBlindSpot(gun).inBlindSpot(
                    enemy.getKinematics().update(getTime()).getBearingCannon(gun)
                    - getBodyHeadingRadians())) {
                engagement = new Engagement(enemy, gun);
                break;
            }
        }

        return engagement;
    }

    @Override
    public void onHitByMine(HitByMineEvent event) {
        Logger.warning("MINE! (", event.getName(), ")");
    }

    /**
     * If we hit another robot, then back-off by sailing away.
     * If we're hit from the back, then place a mine.
     * @param event
     */
    @Override
    public void onHitRobot(HitRobotEvent event) {
        Enemy enemy = shipRepository.getEnemy(event.getName());
        if (enemy != null) {
            double bearing = enemy.getKinematics().getRelativeBearing();
            if (Math.toDegrees(bearing) < -135 || Math.toDegrees(bearing) > 135) {
                Logger.info("Mining ", event.getName());
                Engagement ramEngagement = new Engagement(enemy, Engagement.Gun.BACK);
                placeMine(getEnergy() / 2);
                executeEngagement(ramEngagement);
            }
            if (bearing < 0) {
                Logger.debug("Hit ", event.getName(), ", turning RIGHT");
                setTurnRightRadians(Math.PI);
            } else {
                Logger.debug("Hit ", event.getName(), ", turning LEFT");
                setTurnRightRadians(-Math.PI);
            }
            if (Math.abs(bearing) < Math.PI / 2) {
                Logger.debug("Hit ", event.getName(), " going BACK");
                sailDirection = -1;
            } else {
                Logger.debug("Hit ", event.getName(), " going FORWARD");
                sailDirection = 1;
            }
        }
    }

    /**
     *
     * @param firepower
     * @return the bullet-velocity for given firepower.
     */
    private double getBulletVelocity(double firepower) {
        return 20 - 3 * firepower;
    }

    /**
     * Reset and initialize ship and tracking.
     */
    private void initialize() {
        setupColors();
        self.setBorders((int) getBattleFieldWidth(), (int) getBattleFieldHeight());
        shipRepository = new EnemyRepository(self);
    }

    /**
     * Oooooohhh... perty colors
     */
    private void setupColors() {
        setBodyColor(Color.darkGray);
        setFrontCannonColor(Color.lightGray);
        setBackCannonColor(Color.lightGray);
        setRadarColor(Color.black);
        setMineComponentColor(Color.green);
        setBulletColor(Color.white);
        setScanColor(Color.blue);
    }

    /**
     * When we hit a wall, change direction.
     * Note that this should never happen, since walls are avoided.
     * @param event
     */
    @Override
    public void onHitWall(HitWallEvent event) {
        sailDirection = -1 * sailDirection;
    }

    /**
     * Disco when we win!
     *
     * @param event
     */
    @Override
    public void onWin(WinEvent event) {
        for (int i = 0; i < 10; i++) {
            setBodyColor(Color.red);
            execute();
            setBodyColor(Color.orange);
            execute();
            setBodyColor(Color.yellow);
            execute();
            setBodyColor(Color.green);
            execute();
            setBodyColor(Color.cyan);
            execute();
            setBodyColor(Color.blue);
            execute();
            setBodyColor(Color.magenta);
            execute();
        }
    }

    /**
     * Unfortunately never called (not implemented in the framework?)
     * @param event
     */
    @Override
    public void onScannedProjectile(ScannedProjectileEvent event) {
        // TODO: Never called!
        Logger.info("Projectile-bearing back vs Back: ",
                event.getBearingBackCanon(),
                " <> ",
                event.getBearingFrontCanon());
//        turnShipToMinimizeImpactAngle();
//        keepBulletInTrack();
//        destroyBulletWithFrontOrBackGun();
    }

    /**
     * When a robot dies, remove it from our repository, so it can't be engaged anymore.
     * @param event
     */
    @Override
    public void onRobotDeath(RobotDeathEvent event) {
        shipRepository.remove(event.getName());
    }

    /**
     * When we see an enemy, update our repository with fresh kinematics.
     * @param event
     */
    @Override
    public void onScannedShip(ScannedShipEvent event) {
        Logger.debug("Detected enemy: ", event.getName());
        shipRepository.updateEnemy(event.getName(), new ScannedEnemyEvent(event));
    }

    /**
     * Graphical debugging: Paint lines to engaged ships, and render in red where we *think* other enemies are
     * (indicates tracking accuracy).
     * @param g
     */
    @Override
    public void onPaint(Graphics2D g) {
        for (Enemy enemyShip: shipRepository.getAllEnemiesSortedByDistance()) {
            Kinematics enemyPredicted = enemyShip.getKinematics().update(getTime());
            debugger.renderObject(g, new Color(1, 0, 0, 0.5f), enemyPredicted.getShape());
        }

        if (frontGunEngagement != null) {
            Stroke engStroke = new BasicStroke(5);
            debugger.drawLine(g, engStroke,
                    self.getXFrontCannon(), self.getYFrontCannon(),
                    frontGunEngagement.getTarget().getKinematics().update(getTime()).getX(),
                    frontGunEngagement.getTarget().getKinematics().update(getTime()).getY());
        }
        if (backGunEngagement != null) {
            Stroke engStroke = new BasicStroke(5);
            debugger.drawLine(g, engStroke,
                    self.getXBackCannon(), self.getYBackCannon(),
                    backGunEngagement.getTarget().getKinematics().update(getTime()).getX(),
                    backGunEngagement.getTarget().getKinematics().update(getTime()).getY());
        }
    }
}


