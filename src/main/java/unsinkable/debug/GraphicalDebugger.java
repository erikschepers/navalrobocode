package unsinkable.debug;

import unsinkable.tracking.OwnMachine;

import java.awt.*;
import java.awt.geom.AffineTransform;

/**
 * Paint lines and shapes on the battle-field for graphical debugging our ship.
 */
public class GraphicalDebugger {
    private final OwnMachine self;

    public GraphicalDebugger(OwnMachine self) {
        this.self = self;
    }


    public void renderObject(Graphics2D g, Color color, Shape enemy) {
        g.setColor(color);
        g.fill(enemy);
    }

    /**
     * Creates a shape, based on the shape of OwnMachine, on given position, with given heading, and returns
     * said shape.
     *
     * @param x
     * @param y
     * @param heading
     * @return
     */
    private Shape getMachineShapeFromPositionAndHeading(double x, double y, double heading) {
        AffineTransform translateRotate = new AffineTransform();
        translateRotate.translate(x, y);
        translateRotate.rotate(heading);

        return translateRotate.createTransformedShape(self.getShape());
    }


    /**
     * @return the shape (translated and rotated to its actual position and heading) of
     * our own machine.
     */
    public Shape getMyPosition() {
        return getMachineShapeFromPositionAndHeading(self.getX(), self.getY(), -self.getHeading());
    }

    /**
     * Draws a line from the center of our own machine, to given coordinates.
     *
     * @param g
     * @param x2
     * @param y2
     */
    public void drawLineToPoint(Graphics2D g, double x2, double y2) {
        g.drawLine((int) self.getX(), (int) self.getY(), (int) x2, (int) y2);
    }

    /**
     * Draws a straight line between given coordinates (x1, y1) and (x2, y2).
     *
     * @param g
     * @param engStroke
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     */
    public void drawLine(Graphics2D g, Stroke engStroke, double x1, double y1, double x2, double y2) {
        g.setStroke(engStroke);
        g.drawLine((int) x1, (int) y1, (int) x2, (int) y2);
    }
}
