package unsinkable.debug;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;

/**
 * Basic logging.
 */
public class Logger {
    private static final LogLevel logLevel = LogLevel.INFO;

    public enum LogLevel {
        DEBUG,
        INFO,
        WARNING,
        ERROR
    }

    public static void debug(Object ... args) {
        log(LogLevel.DEBUG, args);
    }

    public static void info(Object ... args) {
        log(LogLevel.INFO, args);
    }

    public static void warning(Object ... args) {
        log(LogLevel.WARNING, args);
    }

    public static void error(Object ... args) {
        log(LogLevel.ERROR, args);
    }

    private static void log(LogLevel level, Object ... args) {
        if (logLevel.ordinal() <= level.ordinal()) {
            StringBuilder sb = new StringBuilder();
            Formatter formatter = new Formatter(sb);
            formatter.format("[%7s] %s - ", level, new SimpleDateFormat("H:m:s.S").format(new Date()));
            for (Object arg: args) {
                sb.append(arg);
            }
            System.out.println(sb.toString());
        }
    }


}
