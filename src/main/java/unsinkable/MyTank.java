package unsinkable;

import robocode.AdvancedRobot;
import robocode.HitWallEvent;
import robocode.ScannedRobotEvent;
import unsinkable.debug.GraphicalDebugger;
import unsinkable.tracking.*;

import java.awt.*;

/**
 * Tank to test built functionality in a Tank battle (non-naval).
 *
 */
public class MyTank extends AdvancedRobot {
    private int direction = 1;
    private final OwnMachine self = new OwnMachine(this);
    private final GraphicalDebugger debugger = new GraphicalDebugger(self);
    private EnemyRepository robotRepository;


    @Override
    public void run() {
        setup();

        while (true) {
            setTurnRadarRight(Double.POSITIVE_INFINITY);
            setTurnRightRadians(Double.POSITIVE_INFINITY);
            setAhead(direction * 4000);
            execute();
        }
    }

    private void setup() {
        robotRepository = new EnemyRepository(self);
        self.setBorders((int) getBattleFieldWidth(), (int) getBattleFieldHeight());
        setAdjustRadarForRobotTurn(true);
    }

    @Override
    public void onHitWall(HitWallEvent event) {
        direction *= -1;
    }

    @Override
    public void onScannedRobot(ScannedRobotEvent event) {
        robotRepository.updateEnemy(event.getName(), new ScannedEnemyEvent(event));
    }


    @Override
    public void onPaint(Graphics2D g) {
        for (Enemy enemy: robotRepository.getAllEnemiesSortedByDistance()) {
            debugger.renderObject(g, new Color(1, 0, 0, 0.5f), enemy.getKinematics().getShape());

            Kinematics kinematics = enemy.getKinematics();
            debugger.drawLineToPoint(g, (int) kinematics.getX(), (int) kinematics.getY());
        }
    }
}
